import json
import ast

registrants = """[
                  {
                    "registrant": {
                      "name": "Lucy Liu",
                      "email": "lucy@liu.com",
                      "phone": "None"
                    }
                  },
                  {
                    "registrant": {
                      "name": "Doug",
                      "email": "doug@emmy.com",
                      "phone": "4564445556"
                    }
                  },
                  {
                    "registrant": {
                      "name": "Uma Thurman",
                      "email": "uma@thurs.com",
                      "phone": "None"
                    }
                  },
                  {
                    "registrant": {
                      "name": "Alice",
                      "email": "alice@brown.com",
                      "phone": "1231112223"
                    }
                  }
                ]"""


contacts = ["Alice Brown / None / 1231112223",
            "Bob Crown / bob@crowns.com / None",
            "Carlos Drew / carl@drewess.com / 3453334445",
            "Doug Emerty / None / 4564445556",
            "Egan Fair / eg@fairness.com / 5675556667"]

leads = ["None / kevin@keith.com / None",
        "Lucy / lucy@liu.com / 3210001112",
        "Mary Middle / mary@middle.com / 3331112223",
        "None / None / 4442223334",
        "None / ole@olson.com / None"]

ContactList = []
LeadList = []

def generateList():
    for c in contacts :
        name, email, phone = c.split(" / ")
        # print (email, name, phone)
        ContactList.append({'name': name, 
                            'email': email, 
                            'phone': phone, })

    for l in leads :
        name, email, phone = l.split(" / ")
        # print (email, name, phone)
        LeadList.append({'name': name, 
                            'email': email, 
                            'phone': phone, })
    return True

def checkEmail(email, datas) :
    # print ("checkEmail")
    is_exist = False
    email_list = [x.get('email') for x in datas]
    if email in email_list :
        is_exist = True

    return is_exist


def checkPhone(phone, datas) :
    # print ("checkPhone")
    is_exist = False
    phone_list = [x.get('phone') for x in datas]
    if phone and phone in phone_list :
        is_exist = True

    return is_exist

def move_leads_to_contact(_filter, data) :
    # print("move_leads_to_contact", data)
    if _filter == "email" :
        email = data.get('email')
        lead_index = next((i for i, item in enumerate(LeadList) if item["email"] == email), None)
        del LeadList[lead_index]
    elif _filter == "phone" :
        phone = data.get('phone')
        lead_index = next((i for i, item in enumerate(LeadList) if item["phone"] == phone), None)
        del LeadList[lead_index]
    else :
        return True

    add_to_contact(data)
    return True

def add_to_contact(data) :
    # print("add_to_contact", data)
    ContactList.append(data)
    return True

def checkRegistrant():
    try:
        list_registrants = json.loads(registrants)
        print("REGISTRANT:", list_registrants)
        print("\n")
        for r in list_registrants :
            registrant = r.get("registrant", False)
            if not registrant :
                continue
            name = registrant.get("name") != "None" and registrant.get("name") or None
            email = registrant.get("email") != "None" and registrant.get("email") or None
            phone = registrant.get("phone") != "None" and registrant.get("phone") or None
            registrant = {'name': name, 'email': email, 'phone': phone, }

            email_in_contacts = checkEmail(email, ContactList)
            # print (name, email, email_in_contacts, "in contacts")
            phone_in_contacts = checkPhone(phone, ContactList)
            # print (name, phone, phone_in_contacts, "in contacts")

            email_in_leads = checkEmail(email, LeadList)
            # print (name, email, email_in_leads, "in leads")
            phone_in_leads = checkPhone(phone, LeadList)
            # print (name, phone, phone_in_leads, "in leads")

            if email_in_contacts :
                pass
            elif phone_in_contacts :
                pass
            elif email_in_leads :
                move_leads_to_contact('email', registrant)
            elif phone_in_leads :
                move_leads_to_contact('phone', registrant)
            else :
                add_to_contact(registrant)

    except e:
        raise e

    return {}

def main() :
    #generate list
    if generateList() :
        print("ORIGIN CONTACT:", ContactList)
        print("ORIGIN LEAD:", LeadList)
        print("\n")

    #check registrant data
    checkRegistrant()

    #new contact and lead
    print("NEW CONTACT:", ContactList)
    print("NEW LEAD:", LeadList)
    print("\n")

    return True


if __name__ == "__main__":
    main()

